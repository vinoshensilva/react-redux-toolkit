import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { TTodo } from "../types/todo";
import { logOut } from "./userSlice";
// import { logOut } from "./userSlice";

// Define an interface for the state
interface TodoState {
  todos: TTodo[];
}

// Define an initial state for your reducer
const initialState: TodoState = {
  todos: [],
};

type TAddTodoPayload = TTodo;

type TRemoveTodoPayload = {
  removeId: string;
};

const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, { payload }: PayloadAction<TAddTodoPayload>) => {
      state.todos.push(payload);
    },
    removeTodo: (state, { payload }: PayloadAction<TRemoveTodoPayload>) => {
      state.todos = state.todos.filter((el) => el.id !== payload.removeId);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(logOut, (state) => {
      state.todos = [];
    });
  },
});

export const { addTodo, removeTodo } = todoSlice.actions;

export default todoSlice.reducer;
