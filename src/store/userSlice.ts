import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { sleep } from "../utils/general";

type TUserInfo = {
  name: string;
  email: string;
};

interface UserState {
  userInfo: TUserInfo | null;
  error: string | null;
  loading: boolean;
}

const initialState: UserState = {
  userInfo: null,
  error: null,
  loading: false,
};

const login = createAsyncThunk<TUserInfo, void, { rejectValue: string }>(
  "data/login",
  async () => {
    await sleep(2000);

    return {
      email: "michael@hotmail.com",
      name: "Michael Test",
    };
  }
);

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    logOut: () => {
      localStorage.clear();
      sessionStorage.clear();

      return initialState;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(login.pending, (state) => {
      state.loading = true;
      state.error = null;
    });
    builder.addCase(
      login.fulfilled,
      (state, action: PayloadAction<TUserInfo>) => {
        state.userInfo = action.payload;
        state.error = null;
      }
    );
    builder.addCase(login.rejected, (state) => {
      state.loading = false;
      state.error = "An error occured";
    });
  },
});

export const { logOut } = userSlice.actions;
export { login };

export default userSlice.reducer;
