// import { useState } from "react";
// import reactLogo from "./assets/react.svg";
// import viteLogo from "/vite.svg";
import "./App.css";
import Greeter from "./components/Greeter";
import { useAppSelector } from "./hooks/redux";
import Login from "./page/Login";
import Todos from "./page/Todos";

function App() {
  const { userInfo } = useAppSelector((state) => state.user);

  return (
    <>
      <Greeter />
      {userInfo ? <Todos /> : <Login />}
    </>
  );
}

export default App;
