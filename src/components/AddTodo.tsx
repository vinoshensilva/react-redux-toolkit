import { useAppDispatch } from "../hooks/redux";
import { addTodo } from "../store/todoSlice";
import { generateId } from "../utils/id";
import { useState } from "react";

const AddTodo = () => {
  const dispatch = useAppDispatch();

  const [text, setText] = useState<string>("");

  const onClickSubmit = () => {
    if (text) {
      dispatch(
        addTodo({
          id: generateId(),
          text,
        })
      );
      setText("");
    }
  };

  return (
    <div>
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <button disabled={!text} onClick={onClickSubmit}>
        Add
      </button>
    </div>
  );
};

export default AddTodo;
