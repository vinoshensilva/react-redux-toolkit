import React from "react";
import { useAppSelector } from "../hooks/redux";

const Greeter = () => {
  const userInfo = useAppSelector((state) => state.user.userInfo);

  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        width: "100%",
        height: 20,
        left: 0,
        backgroundColor: "white",
        color: "black",
        textAlign: "right",
      }}
    >
      Hello, {userInfo ? userInfo.name : " Stranger"}
    </div>
  );
};

export default Greeter;
