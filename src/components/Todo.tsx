import React from "react";

type TodoProps = {
  text: string;
  onClickRemove: () => void;
};

const Todo = ({ onClickRemove, text }: TodoProps) => {
  return (
    <div style={{ display: "flex", gap: 10 }}>
      <div>{text}</div>
      <button onClick={onClickRemove}>Remove</button>
    </div>
  );
};

export default Todo;
