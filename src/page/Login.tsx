import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { login } from "../store/userSlice";

const Login = () => {
  const { error, loading } = useAppSelector((state) => state.user);

  const dispatch = useAppDispatch();

  const onClickLogin = () => {
    dispatch(login());
  };

  return (
    <div>
      {error ? <p style={{ color: "red" }}>{error}</p> : <></>}
      <button onClick={onClickLogin} disabled={loading}>
        {loading ? "Loading" : "Login"}
      </button>
    </div>
  );
};

export default Login;
