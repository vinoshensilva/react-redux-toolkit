import React from "react";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import AddTodo from "../components/AddTodo";
import Todo from "../components/Todo";
import { removeTodo } from "../store/todoSlice";
import { logOut } from "../store/userSlice";

const Todos = () => {
  const todos = useAppSelector((state) => state.todo.todos);

  const dispatch = useAppDispatch();

  return (
    <div>
      <div>Todos</div>

      <AddTodo />

      {todos.map((el) => (
        <Todo
          onClickRemove={() => {
            dispatch(
              removeTodo({
                removeId: el.id,
              })
            );
          }}
          key={el.id}
          text={el.text}
        />
      ))}

      <div style={{ marginTop: 10 }}>
        <button
          onClick={() => {
            dispatch(logOut());
          }}
        >
          Logout
        </button>
      </div>
    </div>
  );
};

export default Todos;
