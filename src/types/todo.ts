export type TTodo = {
  text: string;
  id: string;
};
